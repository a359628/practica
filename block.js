const SHA256 = require("crypto-js/sha256");

class Block {
  constructor(index, data, previousHash = "") {
    this.index = index;
    this.data = data;
    this.previousHash = previousHash;

    this.nounce = 0;
    this.hash = this.createdHash();
  }

  createdHash() {
    const originalChain = `${this.index}|${this.data}|${this.date}|${this.nounce}`;
    return SHA256(originalChain).toString();
  }

  mine(difficulty) {
    while (!this.hash.startsWith(difficulty)) {
      this.nounce++;
      this.hash = this.createdHash();
    }
  }
}
module.exports = Block;